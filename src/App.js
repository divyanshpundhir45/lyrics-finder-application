import React from "react";
import Navbar from "./Components/Layout/Navbar/Navbar";
import styles from "./App.module.scss";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Tracks from "./Components/Tracks/Tracks";
import { Provider } from "./Context";
import Lyric from "./Components/Lyric/Lyric";

function App() {
  return (
    <Provider>
      <Router>
        <div className={styles.root}>
          <Navbar />
          <Switch>
            <Route exact path="/" component={Tracks} />
            <Route
              exact
              path="/lyrics/lyric/:track_id/:commontrack_id"
              component={Lyric}
            />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
