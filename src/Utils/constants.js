export const Languages = [
  {
    country: "USA",
    code: "us",
  },
  {
    country: "India",
    code: "in",
  },
  {
    country: "UK",
    code: "uk",
  },
  {
    country: "Canada",
    code: "ca",
  },
  {
    country: "Pakistan",
    code: "pk",
  },
];
