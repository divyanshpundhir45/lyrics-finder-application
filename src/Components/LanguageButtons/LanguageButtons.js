import React from "react";
import ButtonComponent from "../ButtonComponent/ButtonComponent";

import styles from "./LanguageButtons.module.scss";

const LanguageButtons = ({ countryCode, setcountryCode }) => {
  return (
    <div className={styles.outerDiv}>
      <div className={styles.countryText}>Choose a country.</div>
      <div>
        <ButtonComponent
          countryCode={countryCode}
          setcountryCode={setcountryCode}
        />
      </div>
    </div>
  );
};

export default LanguageButtons;
