import React from "react";

import styles from "./LyricTrack.module.scss";

const LyricTrack = ({ artistName, lyrics, trackName }) => {
  return (
    <div className={styles.outerDiv}>
      <div className={styles.header}>
        <div>
          {trackName} by {artistName}{" "}
        </div>
      </div>
      <div className={styles.lyricsWrapper}>{lyrics}</div>
    </div>
  );
};

export default LyricTrack;
