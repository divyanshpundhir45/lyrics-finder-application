import React from "react";

import { Languages } from "../../Utils/constants";

import Button from "@material-ui/core/Button";

import { makeStyles } from "@material-ui/core/styles";
import styles from "./ButtonComponent.module.scss";

const useStyles = makeStyles({
  root1: {
    backgroundColor: "black",
    color: "white",
    width: "100px",
  },
  root2: {
    backgroundColor: "grey",
    color: "black",
    width: "100px",
  },
});

const ButtonComponent = ({ countryCode, setcountryCode }) => {
  const classes = useStyles();
  return (
    <div className={styles.flex}>
      {Languages.map((language) => (
        <div className={styles.marginLeft}>
          <Button
            className={
              language.code === countryCode ? classes.root2 : classes.root1
            }
            onClick={() => setcountryCode(language.code)}
          >
            {language.country}
          </Button>
        </div>
      ))}
    </div>
  );
};

export default ButtonComponent;
