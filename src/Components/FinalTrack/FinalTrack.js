import React from "react";
import Track from "../Track/Track";

import styles from "./FinalTrack.module.scss";

const FinalTrack = ({ heading, track_list }) => {
  return (
    <div>
      <div className={styles.headingStyling}>{heading}</div>
      <div className={styles.cardStyling}>
        {track_list.map((singletrack, index) => (
          <Track
            track_name={singletrack.track.track_name}
            album_name={singletrack.track.album_name}
            artist_name={singletrack.track.artist_name}
            track_id={singletrack.track.track_id}
            commontrack_id={singletrack.track.commontrack_id}
            key={index}
          />
        ))}
      </div>
    </div>
  );
};

export default FinalTrack;
