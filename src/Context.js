import React, { Component } from "react";
import axios from "axios";

import { Languages } from "./Utils/constants";

const Context = React.createContext();

const key = "e8d30a0da502bce2d36025a202c1646a";
export class Provider extends Component {
  state = {
    track_list: [],
    heading: "Top 10 tracks",
    countryCode: "us",
  };
  componentDidMount() {
    axios
      .get(
        ` https://cors-anywhere.herokuapp.com/https://api.musixmatch.com/ws/1.1/chart.tracks.get?chart_name=top&page=10&page_size=10&country=${this.state.countryCode}&f_has_lyrics=1&apikey=${key}`
      )
      .then((response) => {
        this.setState({ track_list: response.data.message.body.track_list });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export const Consumer = Context.Consumer;
