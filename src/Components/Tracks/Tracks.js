import React, { useEffect, useState } from "react";

import LanguageButtons from "../LanguageButtons/LanguageButtons";
import Loader from "../../widget/Loader/Loader";
import FinalTrack from "../FinalTrack/FinalTrack";
import axios from "axios";

const Tracks = () => {
  const key = "e8d30a0da502bce2d36025a202c1646a";
  const [track_list, settrack_list] = useState([]);
  const [heading, setHeading] = useState("Top 10 tracks");
  const [countryCode, setcountryCode] = useState("us");
  useEffect(() => {
    settrack_list([]);
    axios
      .get(
        ` https://cors-anywhere.herokuapp.com/https://api.musixmatch.com/ws/1.1/chart.tracks.get?chart_name=top&page=10&page_size=10&country=${countryCode}&f_has_lyrics=1&apikey=${key}`
      )
      .then((response) => {
        settrack_list(response.data.message.body.track_list);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [countryCode]);
  return (
    <>
      <LanguageButtons
        countryCode={countryCode}
        setcountryCode={setcountryCode}
      />
      {track_list !== undefined && track_list.length ? (
        <FinalTrack heading={heading} track_list={track_list} />
      ) : (
        <Loader />
      )}
    </>
  );
};

export default Tracks;
