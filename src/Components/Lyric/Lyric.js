import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import LyricTrack from "../LyricTrack/LyricTrack";
import TrackDescription from "../TrackDescription/TrackDescription";
import Loader from "../../widget/Loader/Loader";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core";
import styles from "./Lyric.module.scss";

const useStyles = makeStyles({
  root: {
    backgroundColor: "black",
    color: "white",
    "&:hover": {
      backgroundColor: "grey",
    },
    "text-decoration": "none",
  },
});

const Lyric = (props) => {
  const [trackDetails, setTrackDetails] = useState({});
  const [lyrics, setLyrics] = useState("");
  const [loadingLyrics, setLoadingLyrics] = useState(true);
  const [loadingDetails, setLoadingDetails] = useState(true);
  const key = "e8d30a0da502bce2d36025a202c1646a";
  const classes = useStyles();

  useEffect(() => {
    axios
      .get(
        ` https://cors-anywhere.herokuapp.com/https://api.musixmatch.com/ws/1.1/track.lyrics.get?track_id=${props.match.params.track_id}&apikey=${key}`
      )
      .then((response) => {
        setLyrics(response.data.message.body.lyrics.lyrics_body);
        setLoadingLyrics(false);
      })
      .catch((err) => {
        console.log(err);
        setLoadingLyrics(false);
      });
  }, []);
  useEffect(() => {
    axios
      .get(
        ` https://cors-anywhere.herokuapp.com/https://api.musixmatch.com/ws/1.1/track.get?commontrack_id=${props.match.params.commontrack_id}&apikey=${key}`
      )
      .then((response) => {
        setTrackDetails({
          trackName: response.data.message.body.track.track_name,
          artistName: response.data.message.body.track.artist_name,
          albumName: response.data.message.body.track.album_name,
          explicit: response.data.message.body.track.explicit,
          approvalRating: response.data.message.body.track.track_rating,
        });
        setLoadingDetails(false);
      })
      .catch((err) => {
        console.log(err);
        setLoadingDetails(false);
      });
  }, []);
  return (
    <div>
      <div className={styles.buttonStyling}>
        <Link to="/" className={styles.linkStyle}>
          <Button className={classes.root}> Go back </Button>
        </Link>
      </div>
      {loadingDetails || loadingLyrics ? (
        <Loader />
      ) : (
        <>
          <LyricTrack
            lyrics={lyrics}
            trackName={trackDetails.trackName}
            artistName={trackDetails.artistName}
          />
          <TrackDescription
            trackName={trackDetails.trackName}
            artistName={trackDetails.artistName}
            explicit={trackDetails.explicit}
            approvalRating={trackDetails.approvalRating}
          />
        </>
      )}
    </div>
  );
};

export default Lyric;
