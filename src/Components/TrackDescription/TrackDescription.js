import React from "react";
import Card from "../../widget/Card/Card";

import styles from "./TrackDescription.module.scss";
import tick from "./check.svg";
import play from "./play.svg";
import musicnote from "./music-note.svg";
import swear from "./swear.svg";

const TrackDescription = (props) => {
  const { trackName, artistName, explicit, approvalRating } = props;
  let approved = <div>No</div>;
  if (explicit) approved = <div>Yes</div>;
  return (
    <div className={styles.cardStyles}>
      <div className={styles.header}>Track Description</div>
      <Card className={styles.cardinteriors}>
        <div className={styles.lineWrapper}>
          <img src={musicnote} alt="music" className={styles.svgstyling} />
          <div className={styles.commonFonts}>Song Name - {trackName}</div>
        </div>
        <div className={styles.lineWrapper}>
          <img src={play} alt="play" className={styles.svgstyling} />
          <div className={styles.commonFonts}>Artist Name - {artistName}</div>
        </div>
        <div className={styles.lineWrapper}>
          <img src={swear} alt="swear" className={styles.svgstyling} />
          <div className={styles.approvalRating}>
            Contains explicit Content - {approved}
          </div>
        </div>
        <div className={styles.lineWrapper}>
          <img src={tick} alt="tick" className={styles.svgstyling} />
          <div className={styles.commonFonts}>
            Aprroved by {approvalRating}% of the Users.
          </div>
        </div>
      </Card>
    </div>
  );
};

export default TrackDescription;
