import React from "react";
import styles from "./Navbar.module.scss";

const Navbar = () => {
  return (
    <div className={styles.outerDiv}>
      <div className={styles.textStyle}>Lyric finder</div>
    </div>
  );
};

export default Navbar;
