import React from "react";
import { Link } from "react-router-dom";
import styles from "./Track.module.scss";
import Card from "../../widget/Card/Card";
import Button from "@material-ui/core/Button";
import MusicLogo from "./music.svg";
import AlbumSvg from "./album.svg";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    backgroundColor: "black",
    color: "white",
    "&:hover": {
      backgroundColor: "blue",
    },
    borderRadius: "0px",
    width: "100%",
    "text-transform": "none",
  },
});

const Track = (props) => {
  const classes = useStyles();
  return (
    <Card className={styles.cardStyling}>
      <div className={styles.artistName}>{props.artist_name}</div>
      <div className={styles.songNameSection}>
        <div className={styles.trackSvg}>
          <img src={MusicLogo} alt="music-icon" />
        </div>
        <div className={styles.songName}>
          <span className={styles.spanwrapper}>Song -</span>
          {props.track_name}
        </div>
      </div>
      <div className={styles.songNameSection}>
        <div className={styles.trackSvg}>
          <img src={AlbumSvg} alt="album" className={styles.albumsvg} />
        </div>
        <div className={styles.songName}>
          <span className={styles.spanwrapper}>Album- </span>
          {props.album_name}
        </div>
      </div>
      <Link
        to={`lyrics/lyric/${props.track_id}/${props.commontrack_id}`}
        className={styles.linkStyle}
      >
        <div className={styles.buttonWrapper}>
          <Button className={classes.root}>View Lyrics</Button>
        </div>
      </Link>
    </Card>
  );
};
export default Track;
